/*
 * dronefly.h
 *
 *  Created on: Dec 12, 2012
 *      Author: sherry
 */

#ifndef DRONEFLY_H_INCLUDED
#define DRONEFLY_H_INCLUDED

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>

#include <stdlib.h>
#include <string.h>
#include <cstring>

#include <sys/time.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>

/* Value is non-zero when application is alive, and 0 when it is closing.
 * Defined by the application framework.
 */
#define INFO(_fmt_, args...)   \
printf(_fmt_, ##args)                                        \

/*#define INFO(_fmt_, args...)                                        \
    __android_log_print(ANDROID_LOG_INFO, "ARDrone", _fmt_, ##args)
*/
#define GETPROP(_name_,_val_) __system_property_get((_name_),(_val_))

#define PI 3.1415926

	/* AT constant */
	#define AT_PORT                   5556
	#define AT_BUFFER_SIZE            1024

static pthread_mutex_t at_cmd_lock;
static pthread_t at_thread = 0;
static int32_t at_thread_alive = 1;

static char cmd[AT_BUFFER_SIZE];
unsigned int nb_sequence = 1;

static int at_udp_socket  = -1;
static int overflow = 0;
static unsigned long ocurrent = 0;

//degrees to radians
#define DEG2RAD(x) ((x)*PI/180.0)
//radians to degrees
#define RAD2DEG(x) ((x)/PI*180.0)

typedef float float32_t;




#define PITCH_MIN 0.06
#define PITCH_MAX 0.25
#define ROLL_MIN 0.04
#define ROLL_MAX 0.35
#define GAZ_MIN 0.25
#define GAZ_MAX 0.6
#define YAW_MIN 0.15
#define YAW_MAX 0.6


#define YAW_THRESHOLD PI/36		//5 degrees
#define X_THRESHOLD  0.2	//10cm
#define Z_THRESHOLD 0.2
#define HEIGHT_THRESHOLD 0.1

#define bool_t  int32_t

#define MYKONOS_REFRESH_MS        28
#define REMOTE_SERVER_ADDRESS   "192.168.1.1"

class dronefly{
public:
	//int32_t pitch, roll, gaz, yaw, len, state, mask;


	/************* at_led ****************
	* Description : setting LED animations.
	* anim_id:
	* BLINK_GREEN_RED:					1
	* BLINK_GREEN:						2
	* BLINK_RED:						3
	* BLINK_ORANGE:						4
	* SNAKE_GREEN_RED:					5
	* FIRE:								6
	* STANDARD:							7
	* RED:								8
	* GREEN:							9
	* RED_SNAKE:						10
	* BLANK:							11
	* RIGHT_MISSILE:					12
	* LEFT_MISSILE:						13
	* DOUBLE_MISSILE:					14
	* FRONT_LEFT_GREEN_OTHERS_RED:		15
	* FRONT_RIGHT_GREEN_OTHERS_RED:		16
	* REAR_RIGHT_GREEN_OTHERS_RED:		17
	* REAR_LEFT_GREEN_OTHERS_RED:		18
	* LEFT_GREEN_RIGHT_RED:				19
	* LEFT_RED_RIGHT_GREEN:				20
	* BLINK_STANDARD:					21
	*/
	void at_led(int32_t anim_id, float32_t freqency, int32_t duration)
	{
		float_or_int_t freq;
		freq.f = freqency;

		if (!at_thread)
			return;

		pthread_mutex_lock( &at_cmd_lock );
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*LED=%d,%ld,%ld,%ld\r", nb_sequence++, anim_id, freq.i, duration);
		pthread_mutex_unlock( &at_cmd_lock );

	}

	/************* at_stop ****************
	* Description : landing and destroying the AT thread.
	*/

	void at_stop(void)
	{
		if (!at_thread)
			return;

		at_land();

		at_thread_alive = 0;
		pthread_join(at_thread, NULL);
		at_thread = 0;
		pthread_mutex_destroy(&at_cmd_lock);

		if (at_udp_socket >= 0)
		{
			close(at_udp_socket);
			at_udp_socket = -1;
		}
	}


	/************* at_init ****************
	* Description : Initialize AT process.
	*/
	void at_init(void)
	{

		/* Initialize mutex */
		if (pthread_mutex_init(&at_cmd_lock, NULL) != 0)
		{
		   INFO("AT mutex init failed: %s\n", strerror(errno));
		   return;
		}

		memset(cmd, 0x0, AT_BUFFER_SIZE);
		// atay on the ground, do nothing
		snprintf(cmd, AT_BUFFER_SIZE, "AT*PCMD=%d,0,0,0,0,0\rAT*REF=%d,290717696\r", 1, 1);

		// create AT cmds thread
		at_thread_alive = 1;
		if (pthread_create(&at_thread, NULL, at_cmds_loop, NULL))
		{
			INFO("pthread_create: %s\n", strerror(errno));
		}
	}



	/************* at_set_flat_trim ****************
	* Description : Calibration of the ARDrone.
	*/
	void at_set_flat_trim(void)
	{
		if (!at_thread)
			return;

		pthread_mutex_lock( &at_cmd_lock );
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*FTRIM=%d\r", nb_sequence++);
		pthread_mutex_unlock( &at_cmd_lock );

		sleep(1);
	}


	/************* at_takeoff ****************
	* Description : Takeoff
	*/
	void at_takeoff(void)
	{
		if (!at_thread)
			return;

		at_set_flat_trim();

		pthread_mutex_lock( &at_cmd_lock );
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*PCMD=%d,0,%ld,%ld,%ld,%ld\rAT*REF=%d,290718208\r",
								nb_sequence++, 0, 0, 0, 0, nb_sequence++);
		pthread_mutex_unlock( &at_cmd_lock );

		sleep(5);
		at_hover();
	}


	/************* at_land ****************
	* Description : Landing
	*/
	void at_land(void)
	{
		if (!at_thread)
			return;

		pthread_mutex_lock(&at_cmd_lock);
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*REF=%d,290717696\r", nb_sequence++);
		pthread_mutex_unlock(&at_cmd_lock);

		sleep(5);
	}



	/************* at_hover ****************
	* Description : Stabilised hovering
	*/
	void at_hover(void)
	{
		if (!at_thread)
			return;

		pthread_mutex_lock(&at_cmd_lock);
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*PCMD=%d,0,%ld,%ld,%ld,%ld\r", nb_sequence++, 0, 0, 0, 0);
		pthread_mutex_unlock(&at_cmd_lock);
	}


	/************* at_set_radiogp_input ****************
	* Description : Fill struct radiogp_cmd,
	* used with at_cmds_loop function.
	* pitch : y-axis (rad) (-1, +1)
	* roll : x-axis (rad) (-1, +1) positive rolls right
	* gaz :  altitude (mm/s) (-1, +1) positive moves up
	* yaw : z-axis (rad/s) (-1, +1)
	*/
	void at_set_radiogp_input(float32_t pitchf, float32_t rollf, float32_t gazf, float32_t yawf)
	{
		if (!at_thread)
			return;

		float_or_int_t pitch, roll, yaw, gaz;
		pitch.f = pitchf;
		roll.f = rollf;
		yaw.f = yawf;
		gaz.f = gazf;

	    pthread_mutex_lock(&at_cmd_lock);
		memset(cmd, 0x0, AT_BUFFER_SIZE);
		snprintf(cmd, AT_BUFFER_SIZE, "AT*PCMD=%d,1,%d,%d,%d,%d\r", nb_sequence++, roll.i, pitch.i, gaz.i, yaw.i);
		pthread_mutex_unlock( &at_cmd_lock );
	}

private:
	typedef union _float_or_int_t {
	  float32_t f;
	  int32_t   i;
	} float_or_int_t;

	typedef struct _radiogp_cmd_t {
	  int32_t pitch;
	  int32_t roll;
	  int32_t gaz;
	  int32_t yaw;
	} radiogp_cmd_t;


	int8_t *buffer;


	inline int get_mask_from_state( uint32_t state, uint32_t mask )
	{
	    return state & mask ? true : false;
	}

	static unsigned long get_time_ms(void)
	{
		struct timeval tv;
		gettimeofday(&tv, NULL);
		return (tv.tv_sec*1000 + tv.tv_usec/1000);
	}

	static void send_command()
	{
		unsigned long current;

		current = get_time_ms();

		//Send AT command
		at_write ((int8_t*)cmd, strlen (cmd));

		// check 30 ms overflow
		if (current > ocurrent + 30)
		{
			overflow += current - ocurrent - MYKONOS_REFRESH_MS;
		}
		ocurrent = current;
	}


	static void* at_cmds_loop(void *arg)
	{
		unsigned long current, deadline;

		INFO("AT commands thread starting (thread=%d)...\n", (int)pthread_self());

		ocurrent = get_time_ms();

		INFO("attempting to boot drone...\n");
		boot_drone();

	    while (at_thread_alive)
		{
			// compute next loop iteration deadline
			deadline = get_time_ms() + MYKONOS_REFRESH_MS;

			// send pilot command
			send_command();

			// sleep until deadline
			current = get_time_ms();
			if (current < deadline)
			{
				usleep(1000*(deadline-current));
			}
		}

		INFO("AT commands thread stopping\n");
	    return NULL;
	}

	static void at_write(int8_t *buffer, int32_t len)
	{
		struct sockaddr_in to;
		int32_t flags;

		pthread_mutex_lock( &at_cmd_lock );
		if( at_udp_socket < 0 )
		{
			struct sockaddr_in at_udp_addr;

			memset( (char*)&at_udp_addr, 0, sizeof(at_udp_addr) );

			at_udp_addr.sin_family      = AF_INET;
			at_udp_addr.sin_addr.s_addr = INADDR_ANY;
			at_udp_addr.sin_port        = htons( AT_PORT + 100 );

			at_udp_socket = socket( AF_INET, SOCK_DGRAM, 0 );

			if(at_udp_socket >= 0)
			{
				flags = fcntl(at_udp_socket, F_GETFL, 0);
				if( flags >= 0 )
				{
					flags |= O_NONBLOCK;
					flags = fcntl(at_udp_socket, F_SETFL, flags );
				}
				else
					INFO("Get Socket Options failed\n");

				if (bind(at_udp_socket, (struct sockaddr*)&at_udp_addr, sizeof(struct sockaddr)) < 0)
					INFO ("at_write:bind: %s\n", strerror(errno));
			}
		}

		if(at_udp_socket >= 0)
		{
			int res;

			memset( (char*)&to, 0, sizeof(to) );
			to.sin_family       = AF_INET;
			to.sin_addr.s_addr  = inet_addr(REMOTE_SERVER_ADDRESS);
			to.sin_port         = htons (AT_PORT);

	// send commands to drone

			res = sendto( at_udp_socket, (char*)buffer, len, 0, (struct sockaddr*)&to, sizeof(to) );
		}
	    pthread_mutex_unlock( &at_cmd_lock );
	}


	static void boot_drone(void)
	{
		char cmds[AT_BUFFER_SIZE];
		snprintf(cmds, AT_BUFFER_SIZE, "AT*CONFIG=%d,\"general:navdata_demo\",\"FALSE\"\r", nb_sequence++);

			at_write((int8_t*)cmds, strlen (cmds));
	}

	int util_timestamp_int()
	{
	  static struct timeval tv1;
	  struct timeval tv2;
	  if(tv1.tv_usec==0 && tv1.tv_sec==0) gettimeofday(&tv1, NULL);
	  gettimeofday(&tv2, NULL);
	  return (int)(tv2.tv_sec-tv1.tv_sec)*1000000+(int)(tv2.tv_usec-tv1.tv_usec);
	}


	double util_timestamp()
	{
		unsigned int first = 1;
		struct timeval tv;

		if(first)
		{
			first = 0;
			//offset = timeNow;
			struct timeval now;
			now.tv_sec=0;
			now.tv_usec=0;
			int rc = settimeofday(&now, NULL);
			if(rc!=0)
				printf("time not set %d\n", errno);
		}

		gettimeofday(&tv, NULL);
		double timeNow = (double)tv.tv_sec+((double)tv.tv_usec)/1000000;

		return timeNow;// - offset;
	}

};

#endif // !DRONEFLY_H_INCLUDED



