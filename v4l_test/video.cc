#include <stdlib.h>
#include <string>
#include <stdio.h>
#include <string.h>

#include "v4l2buffer.h"
//#include "videosource.h"
//#include "timer.h"
//#include "config.h"
#include "v4l2frame.h"
//#include "colourspaces.h"



#define V4L2BUFFERS 3
#define WIDTH 768
#define HEIGHT 576
#define DEPTH 8

using namespace CVD;

// typedef unsigned char yuv420p;
FILE *file_fd;

int main()
{
	// char *devname = "/dev/video1";
	int image_size = WIDTH* HEIGHT* DEPTH;
	// create v4l buffer, initialize it
	V4L2BufferT<YC> v4l_buffer("/dev/video1", 0, V4L2BBMselect);
	
	while(1)
	{
	 // get video frames
	 VideoFrame<YC> *frame = v4l_buffer.get_frame();
	
	//save frame to file 
	fwrite(frame, image_size, 1, file_fd);	
	// put frame back	
	v4l_buffer.put_frame(frame);

	}
}
