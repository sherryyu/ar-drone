/*
 * main_sample.cpp
 *
 *  Created on: Jan 3, 2013
 *      Author: sherry
 */

/*    drone.c
loop to awake drone :
    AT*CONFIG=seqnum,"general:navdata_demo","TRUE"

set unicast mode on :
    sendto navdata_socket, &one, 4
    Wed Oct 26 12:15:54 CEST 2011 rp
        skeleton for ardrone navdata

gcc ardrone1.c -o ardrone1 -I /home/iris2/ARDrone_SDK_Version_1_8_20110726/ARDroneLib/Soft/Common/ -I /home/iris2/ARDrone_SDK_Version_1_8_20110726/ARDroneLib/VP_SDK/ -I /home/iris2/ARDrone_SDK_Version_1_8_20110726/ARDroneLib/Soft/Lib/

*/

#include "navdata_common.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>

#define NAVDATA_PORT         5554
#define AT_PORT             5556
#define NAVDATA_BUFFER_SIZE     2048
#define WIFI_MYKONOS_IP     "192.168.1.1"

int seq=0;
char msg[NAVDATA_BUFFER_SIZE];

int     at_socket = -1,         //sendto
    navdata_socket = -1;   //recvfrom

struct sockaddr_in
    pc_addr,   //INADDR_ANY
    drone_at,  //send at addr
    drone_nav, //send nav addr
    from;

using namespace std;

void awake()
{
char command[256];

while(1)
    {
    if(seq<2)
    sprintf(command,"AT*CONFIG=%d,\"general:navdata_demo\",\"TRUE\"\r",seq);
    else
    sprintf(command,"AT*COMWDG=%d\r",seq);
    sendto(at_socket, command, strlen(command), 0, (struct sockaddr*)&drone_at, sizeof(drone_at) );
    seq++;
    usleep(100000); //shoud be less than 0.5s to get all datas ?
    }
}

int main()
{
    navdata_t *data;
    int l,size;
    int32_t one = 1,zero=0;

   // printf("drone v1.0.2\n");

    if((at_socket = socket (AF_INET, SOCK_DGRAM, 0)) < 0){
       printf ("at_socket error: %s\n", strerror(errno));
       goto fail;
    };

    if((navdata_socket = socket (AF_INET, SOCK_DGRAM, 0)) < 0){
       printf ("navdata_socket: %s\n", strerror(errno));
       goto fail;
    };

    //for recvfrom
    pc_addr.sin_family = AF_INET;
    pc_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    pc_addr.sin_port = htons(NAVDATA_PORT + 100);

    //for sendto AT
    drone_at.sin_family = AF_INET;
    drone_at.sin_addr.s_addr = inet_addr(WIFI_MYKONOS_IP);
    drone_at.sin_port = htons(AT_PORT);

    //for sendto navadata init
    drone_nav.sin_family = AF_INET;
    drone_nav.sin_addr.s_addr = inet_addr(WIFI_MYKONOS_IP);
    drone_nav.sin_port = htons(NAVDATA_PORT);

    if(fork()) awake();

    if(bind( navdata_socket, (struct sockaddr *)&pc_addr, sizeof(pc_addr)) < 0){
       printf ("bind: %s\n", strerror(errno));
       goto fail;
    };

    //set unicast mode on
    sendto(navdata_socket, &one, 4, 0, (struct sockaddr *)&drone_nav, sizeof(drone_nav));
    while ( 1 ) {
        size=0;
        size = recvfrom ( navdata_socket, &msg[0], NAVDATA_BUFFER_SIZE, 0x0, (struct sockaddr *)&from, (socklen_t *)&l);
        //printf("\33[2J\nread %d data ",size);
        //for(l=0;l<size;l++) printf("%02x ",0xff & msg[l]);

    data=(navdata_t *)msg;
    printf("header %x battery %d alt %d vx %f theta %f",
        data->header,
        ((navdata_demo_t*)((data->options)))->vbat_flying_percentage,
        ((navdata_demo_t*)((data->options)))->altitude,
        ((navdata_demo_t*)((data->options)))->vx,
        ((navdata_demo_t*)((data->options)))->theta
         );

           fflush(stdout);
        //sleep(1);
    }

fail:
    return 0;
}



