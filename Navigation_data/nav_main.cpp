/*    drone.c

loop to awake drone :
    AT*CONFIG=seqnum,"general:navdata_demo","TRUE"

set unicast mode on :
    sendto navdata_socket, &one, 4

    Wed Oct 26 12:15:54 CEST 2011 rp
        skeleton for ardrone navdata
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "app.h"

FILE *log_file;

using namespace std;

int main()
{

	log_file = fopen("Sherry_Nav_log.csv", "wb");
    navdata_run();
    fprintf(log_file,"Angles %.2lf %.2lf %.2lf \n",helidata.phi,helidata.psi,helidata.theta);
    fprintf(log_file,"Speeds %.2lf %.2lf %.2lf \n",helidata.vx,helidata.vy,helidata.vz);
    fprintf(log_file,"Battery %.0lf \n",helidata.battery);

    return 0;
}
