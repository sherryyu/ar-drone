/*
 * util.h
 *
 *  Created on: Dec 11, 2012
 *      Author: sherry
 */

#ifndef UTIL_H_
#define UTIL_H_
#include "app.h"

double util_timestamp();
int util_timestamp_int();

#endif /* UTIL_H_ */
