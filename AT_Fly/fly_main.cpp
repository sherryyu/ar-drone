/*
 * fly_main.cpp
 *
 *  Created on: Dec 12, 2012
 *      Author: sherry
 */
/*
 * fly_main.cpp
 *
 *  Created on: Dec 7, 2012
 *      Author: sherry
 */
#include <termios.h> // POSIX terminal control definitions
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include "app.h"
#include "util.h"


using namespace std;

bool flying = false;


int main()
{
	// create AT_Fly object
	AT_Fly Drone;

	// start program.elf
	int rc = system("(../../bin/program.elf ${PELF_ARGS}; gpio 181 -d ho 1) &");
	sleep(7);
	printf("Return code from program.elf = %i\n", rc);
	// init control port

	Drone.at_init();

	Drone.at_takeoff();

	flying = true;
	sleep(2);
	Drone.at_set_radiogp_input(PITCH_MIN, 0, 0, 0);
	sleep(2);
	Drone.at_set_radiogp_input(0, ROLL_MIN, 0, 0);
	sleep(2);
	Drone.at_set_radiogp_input(0, 0, GAZ_MIN, 0);
		sleep(2);
		Drone.at_set_radiogp_input(0, 0, 0, YAW_MIN);
			sleep(2);



	Drone.at_stop();

}







