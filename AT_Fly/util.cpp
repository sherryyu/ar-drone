/*
 * util.cpp
 *
 *  Created on: Dec 12, 2012
 *      Author: sherry
 */
/*
 * util.cpp
 *
 *  Created on: Dec 11, 2012
 *      Author: sherry
 */

#include <stdio.h>   /* Standard input/output definitions */
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <errno.h>

#include "util.h"
#include "app.h"

using namespace std;

//return timestamp in microseconds since first call to this function
int util_timestamp_int()
{
  static struct timeval tv1;
  struct timeval tv2;
  if(tv1.tv_usec==0 && tv1.tv_sec==0) gettimeofday(&tv1, NULL);
  gettimeofday(&tv2, NULL);
  return (int)(tv2.tv_sec-tv1.tv_sec)*1000000+(int)(tv2.tv_usec-tv1.tv_usec);
}

//return timestamp in seconds with microsecond resolution
double offset;
unsigned int first = 1;
struct timeval tv;

double util_timestamp()
{
	if(first)
	{
		first = 0;
		//offset = timeNow;
		struct timeval now;
		now.tv_sec=0;
		now.tv_usec=0;
		int rc = settimeofday(&now, NULL);
		if(rc!=0)
			printf("time not set %d\n", errno);
	}

	gettimeofday(&tv, NULL);
	double timeNow = (double)tv.tv_sec+((double)tv.tv_usec)/1000000;

	return timeNow;// - offset;
}





