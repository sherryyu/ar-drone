/**
 *  \file     navdata_common.h
 *  \brief    Common navdata configuration
 *  \author   Sylvain Gaeremynck <sylvain.gaeremynck@parrot.com>
 */

#ifndef _NAVDATA_H_
#define _NAVDATA_H_

/*------------------------------------------ NAVDATA STRUCTURES DECLARATIONS ---------------------------------------------------------------*/

#include <sys/types.h>
#include <stdint.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#if defined(_MSC_VER)
#define _ATTRIBUTE_PACKED_
/* Asks Visual C++ to pack structures from now on*/
#pragma pack(1)
#else
#define _ATTRIBUTE_PACKED_  __attribute__ ((packed))
#endif

typedef float float32_t;
#define bool_t  int32_t
#define NAVDATA_PORT         5554
#define AT_PORT             5556
#define NAVDATA_BUFFER_SIZE     2048
#define WIFI_MYKONOS_IP     "192.168.1.1"

struct sockaddr_in
pc_addr,   //INADDR_ANY
drone_at,  //send at addr
drone_nav, //send nav addr
from;


typedef struct _matrix33_t
{
	float32_t m11;
	float32_t m12;
	float32_t m13;
	float32_t m21;
	float32_t m22;
	float32_t m23;
	float32_t m31;
	float32_t m32;
	float32_t m33;
} matrix33_t;

typedef struct _vector31_t {
	union {
		float32_t v[3];
		struct
		{
			float32_t x;
			float32_t y;
			float32_t z;
		};
	};
} vector31_t;

typedef union _vector21_t {
	float32_t v[2];
	struct
	{
		float32_t x;
		float32_t y;
	};
} vector21_t;

typedef struct _navdata_option_t {
	uint16_t  tag;
	uint16_t  size;
#if defined _MSC_VER || defined (__ARMCC_VERSION)
	/* Do not use flexible arrays (C99 feature) with these compilers */
	uint8_t   data[1];
#else
	uint8_t   data[];
#endif
} navdata_option_t;


/**
 * @brief Navdata structure sent over the network.
 */
typedef struct _navdata_t {
	uint32_t    header;			/*!< Always set to NAVDATA_HEADER */
	uint32_t    ardrone_state;    /*!< Bit mask built from def_ardrone_state_mask_t */
	uint32_t    sequence;         /*!< Sequence number, incremented for each sent packet */
	bool_t      vision_defined;

	navdata_option_t  options[1];
}_ATTRIBUTE_PACKED_ navdata_t;


/**
 * All navdata options can be extended (new values AT THE END) except navdata_demo whose size must be constant across versions
 * New navdata options may be added, but must not be sent in navdata_demo mode unless requested by navdata_options.
 */

/*----------------------------------------------------------------------------*/
/**
 * @brief Minimal navigation data for all flights.
 */
typedef struct _navdata_demo_t {
	uint16_t    tag;					  /*!< Navdata block ('option') identifier */
	uint16_t    size;					  /*!< set this to the size of this structure */

	uint32_t    ctrl_state;             /*!< Flying state (landed, flying, hovering, etc.) defined in CTRL_STATES enum. */
	uint32_t    vbat_flying_percentage; /*!< battery voltage filtered (mV) */

	float32_t   theta;                  /*!< UAV's pitch in milli-degrees */
	float32_t   phi;                    /*!< UAV's roll  in milli-degrees */
	float32_t   psi;                    /*!< UAV's yaw   in milli-degrees */

	int32_t     altitude;               /*!< UAV's altitude in centimeters */

	float32_t   vx;                     /*!< UAV's estimated linear velocity */
	float32_t   vy;                     /*!< UAV's estimated linear velocity */
	float32_t   vz;                     /*!< UAV's estimated linear velocity */

	uint32_t    num_frames;			  /*!< streamed frame index */ // Not used -> To integrate in video stage.

	// Camera parameters compute by detection
	matrix33_t  detection_camera_rot;   /*!<  Deprecated ! Don't use ! */
	vector31_t  detection_camera_trans; /*!<  Deprecated ! Don't use ! */
	uint32_t	  detection_tag_index;    /*!<  Deprecated ! Don't use ! */

	uint32_t	  detection_camera_type;  /*!<  Type of tag searched in detection */

	// Camera parameters compute by drone
	matrix33_t  drone_camera_rot;		  /*!<  Deprecated ! Don't use ! */
	vector31_t  drone_camera_trans;	  /*!<  Deprecated ! Don't use ! */
}_ATTRIBUTE_PACKED_ navdata_demo_t;

class navdata{
public:

	uint32_t battery, altitude;
	float32_t vx, vy, vz, psi, phi, theta;

	void start()
	{
		nav_file = fopen("Navdata.csv", "wb");
			if(nav_file==NULL)
			{
				printf("File open failed\n");
			}

		init();
		fprintf(nav_file,"Init complete... \n");

		if(bind( navdata_socket, (struct sockaddr *)&pc_addr, sizeof(pc_addr)) < 0){
			std::cout << "bind: %s\n" << strerror(errno) << std::endl;
			exit(0);
		};
	}

	void update()
	{
		int32_t one = 1,zero=0;
		//set unicast mode on
		sendto(navdata_socket, &one, 4, 0, (struct sockaddr *)&drone_nav, sizeof(drone_nav));
		for (int i = 0; i < 40; i++)
		{
			size=0;
			size = recvfrom ( navdata_socket, &msg[0], NAVDATA_BUFFER_SIZE, 0x0, (struct sockaddr *)&from, (socklen_t *)&l);
			data=(navdata_t *)msg;
			battery = ((navdata_demo_t*)((data->options)))->vbat_flying_percentage;
			altitude = ((navdata_demo_t*)((data->options)))->altitude;
			vx = ((navdata_demo_t*)((data->options)))->vx;
			vy = ((navdata_demo_t*)((data->options)))->vy;
			vz = ((navdata_demo_t*)((data->options)))->vz;
			phi = ((navdata_demo_t*)((data->options)))->phi;
			psi = ((navdata_demo_t*)((data->options)))->psi;
			theta = ((navdata_demo_t*)((data->options)))->theta;

			fflush(stdout);
			usleep(500);
		}
	}


private:
	int seq;
	char msg[NAVDATA_BUFFER_SIZE];
	int l,size;
	navdata_t *data;
	int at_socket, navdata_socket;
	FILE *nav_file;

	void init()
	{

		navdata_socket = -1;   //recvfrom

		if((navdata_socket = socket (AF_INET, SOCK_DGRAM, 0)) < 0){
			std::cout << "navdata_socket: %s\n" << strerror(errno) << std::endl;
			exit(0);
		};

		//for recvfrom
		pc_addr.sin_family = AF_INET;
		pc_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		pc_addr.sin_port = htons(NAVDATA_PORT + 100);

		//for sendto navadata init
		drone_nav.sin_family = AF_INET;
		drone_nav.sin_addr.s_addr = inet_addr(WIFI_MYKONOS_IP);
		drone_nav.sin_port = htons(NAVDATA_PORT);
	}

};




#endif // _NAVDATA_H_

