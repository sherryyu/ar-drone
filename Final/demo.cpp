/*
 * demo.cpp
 *
 *  Created on: Feb 22, 2013
 *      Author: sherry
 */


#include <dronefly.h>
#include <navdata.h>

using namespace std;
FILE *log_file;


// open a log file to record navigation data
void open_log(){

	log_file = fopen("Sherry_log.csv", "wb");
	if(log_file==NULL)
	{
		printf("File open failed\n");
	}
	fprintf(log_file,"Start \n");

}

// write to log file with index message
void write_log(navdata ndata, char *msg){

	// update navigation data each time before printing to file
	ndata.update();
	// index message
	fprintf(log_file,msg);
	// orientation
	fprintf(log_file,"\n psi %f \t",ndata.psi);
	fprintf(log_file,"phi %f \t",ndata.phi);
	fprintf(log_file,"theta %f \n",ndata.theta);
	// altitude
	fprintf(log_file,"alt %u \n",ndata.altitude);
	// speed
	fprintf(log_file,"vx %f \t",ndata.vx);
	fprintf(log_file,"vy %f \t",ndata.vy);
	fprintf(log_file,"vz %f \t",ndata.vz);
	// battery
	fprintf(log_file,"batt %u \n \n",ndata.battery);

}

int main(){

	open_log();

	// start program.elf

	int rc = system("(../../bin/program.elf ${PELF_ARGS}; gpio 181 -d ho 1) &");
	sleep(7);
	printf("Return code from program.elf = %i\n", rc);

	// create navigation and fly object
	navdata ndata;
	dronefly drone;

	// init AT thread and navigation data transfer
	drone.at_init();
	drone.at_navinit();

	// init navigation thread
	ndata.start();

	// update navigation data
	ndata.update();

	// take off
	drone.at_takeoff();
	write_log(ndata, "take off");

	// yaw 30 degrees in 1 second
	drone.at_set_radiogp_input(0, 0, 0, 0.3);
	sleep(1);
	write_log(ndata, "yaw 30 degrees");

	// hover for 2 seconds
	drone.at_hover();
	sleep(1);
	write_log(ndata, "hover");

	// fly 30 cm higher
	drone.at_set_radiogp_input(0, 0, 0.3, 0);
	sleep(1);
	write_log(ndata, "altitude 0.3m");

	// roll 1m in 1 second
	drone.at_set_radiogp_input(0, 0.1, 0, 0);
	sleep(1);
	write_log(ndata, "roll 1m");

	// pitch 1m in 1 second
	drone.at_set_radiogp_input(0.1, 0, 0, 0);
	sleep(1);
	write_log(ndata, "pitch 1m");

	// land
	drone.at_stop();
	write_log(ndata, "stopped");

}

